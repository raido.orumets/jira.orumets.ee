import axios from 'axios';
import https from 'https';

class Request {
    constructor() {

    }

    makeRequest() {
        let agent = new https.Agent({
            rejectUnauthorized: false
        });

        axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

        axios.get('https://cloud.orumets.ee', {
            mode: 'no-cors',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
            },
            withCredentials: true,
            credentials: 'same-origin',
            crossdomain: true,
            httpsAgent: agent
        }).then(res => {
            console.log(res);
            return res;
        }).catch(error => {
            console.log('error', error);
            return error;
        });
    }
}

export default Request;
